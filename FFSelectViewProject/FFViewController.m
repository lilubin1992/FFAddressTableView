//
//  FFViewController.m
//  FFSelectViewProject
//
//  Created by ZhanyaaLi on 16/8/18.
//  Copyright © 2016年 FaceFace_Lilubin. All rights reserved.
//

#import "FFViewController.h"
#import "FFSwipableViewController.h"

#define kLocalSelectedStr @"请选择"

@interface FFViewController ()

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (nonatomic, strong) FFSwipableViewController *swaController;

@end

@implementation FFViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(showSelector)];
    tap.numberOfTapsRequired = 1;
    tap.numberOfTouchesRequired = 1;
    _titleLabel.userInteractionEnabled = YES;
    [_titleLabel addGestureRecognizer:tap];
    [self _configureSelectController];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)_configureSelectController {
    NSArray *titles = [NSArray arrayWithObjects:kLocalSelectedStr, kLocalSelectedStr, kLocalSelectedStr, kLocalSelectedStr, kLocalSelectedStr, nil];
    NSMutableArray *vcs = [NSMutableArray array];
    for (int i = 0; i < 5; i++) {
        UIViewController *vc = [UIViewController new];
        [vcs addObject:vc];
    }
//    __weak typeof(self) weakSelf = self;
    _swaController = [[FFSwipableViewController alloc] initWithTitle:@"选择地址" andSubTitles:titles];
    _swaController.view.alpha = 0;
    _swaController.selectDoneAction = ^{
//        NSLog(@"选择完成");
    };
    _swaController.selectDoneAddress = ^(NSString *resultStr) {
        NSLog(@"选择的地址:%@", resultStr);
        [_titleLabel setText:resultStr];
    };
}

- (void)showSelector {
    NSLog(@"Show Selector");
    UIWindow *window = [[UIApplication sharedApplication] keyWindow];
    //window.windowLevel = UIWindowLevelAlert;
    [window addSubview:_swaController.view];
    [window bringSubviewToFront:_swaController.view];
    [UIView animateWithDuration:0.5 animations:^{
        _swaController.view.alpha = 1.0;
    }];
}


@end
